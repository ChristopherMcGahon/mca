package com.ebookfrenzy.project;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Created by Chris on 28/02/2017.
 */

public class PassingIDExample extends Activity {



    String passedVar = null;
    private TextView passedView = null;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.passed_id_example);


        passedVar= getIntent().getStringExtra(MainActivity.ID_EXTRA);
        passedView=(TextView)findViewById(R.id.passed);
        passedView.setText("You Clicked Item ID: " + passedVar);
    }
}
